import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/barberia',
    pathMatch: 'full'
  },
  {
    path: 'barberia',
    component: HomeComponent
  },
  {
    path: 'citas/:id',
    component: AppointmentComponent
  },
  {
    path: '**',
    redirectTo: '/barberia'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
