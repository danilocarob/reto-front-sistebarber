export interface Appointment {
    id?: number,
    nombre_completo?: string,
    numero_documento?: number,
    fecha_hora?: string,
    telefono?: string,
    barberia_id?: number
};