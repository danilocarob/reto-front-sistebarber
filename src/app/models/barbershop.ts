export interface Barbershop {
    id?: number,
    nombre?: string,
    direccion?: string,
    telefono?: string,
    estado?: string
};