import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Barbershop } from '../models/barbershop';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BarbershopService {

  private readonly API_URI = environment.api
  public newBarbershop$: BehaviorSubject<any> = new BehaviorSubject('undefined');
  public updatedBarbershop$: BehaviorSubject<any> = new BehaviorSubject('');

  constructor(private http: HttpClient) { }

  getAllBabershop() {
    return this.http.get(`${this.API_URI}/barbershop`);
  }

  getBarbershop(id: string) {
    return this.http.get(`${this.API_URI}/barbershop/${id}`);
  }

  deleteBarbershop(id: string) {
    return this.http.delete(`${this.API_URI}/barbershop/${id}`);
  }

  saveBarbershop(barbershop: Barbershop): Observable<Barbershop> {
    return this.http.post(`${this.API_URI}/barbershop`, barbershop);
  }

  updateBarbershop(id: string|number, updatedBarbershop: Barbershop): Observable<Barbershop> {
    return this.http.put(`${this.API_URI}/barbershop/${id}`, updatedBarbershop);
  }
}
