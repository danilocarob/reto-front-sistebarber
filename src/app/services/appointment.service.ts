import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Appointment } from '../models/appointment';

@Injectable({
  providedIn: 'root',
})
export class AppointmentService {
  private readonly API_URI = environment.api;
  public newAppointment$: BehaviorSubject<any> = new BehaviorSubject(
    'undefined'
  );
  public updatedAppointment$: BehaviorSubject<any> = new BehaviorSubject('');

  constructor(private http: HttpClient) {}

  getAllAppointment() {
    return this.http.get(`${this.API_URI}/appointment`);
  }

  getAppointment(id: string) {
    return this.http.get(`${this.API_URI}/appointment/${id}`);
  }

  getAppointmentExist(appointment: any) {
    return this.http.post(`${this.API_URI}/appointment/exist`, appointment);
  }

  getAppointmentsById(id: string) {
    return this.http.get(`${this.API_URI}/appointment/barbershop/${id}`);
  }

  deleteAppointment(id: string) {
    return this.http.delete(`${this.API_URI}/appointment/${id}`);
  }

  saveAppointment(appointment: Appointment): Observable<Appointment> {
    return this.http.post(`${this.API_URI}/appointment`, appointment);
  }

  updateAppointment(
    id: string | number,
    updatedAppointment: Appointment
  ): Observable<Appointment> {
    return this.http.put(
      `${this.API_URI}/appointment/${id}`,
      updatedAppointment
    );
  }
}
