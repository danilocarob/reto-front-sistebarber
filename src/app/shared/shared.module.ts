import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './organisms/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { BarbershopFormComponent } from './organisms/barbershop-form/barbershop-form.component';
import { BarbershopListComponent } from './organisms/barbershop-list/barbershop-list.component';
import { BabershopCardComponent } from './templates/babershop-card/babershop-card.component';
import { BabershopCardListComponent } from './templates/babershop-card-list/babershop-card-list.component';
import { LoaderComponent } from './atoms/loader/loader.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputValidDirective } from './directives/input-valid.directive';
import { ModalBarbershopEditComponent } from './organisms/modal-barbershop-edit/modal-barbershop-edit.component';
import { AppointmentCardComponent } from './templates/appointment-card/appointment-card.component';
import { AppointmentFormComponent } from './organisms/appointment-form/appointment-form.component';
import { AppointmentCardListComponent } from './templates/appointment-card-list/appointment-card-list.component';
import { AppointmentListComponent } from './organisms/appointment-list/appointment-list.component';
import { ModalAppointmentEditComponent } from './organisms/modal-appointment-edit/modal-appointment-edit.component';
import { ButtonComponent } from './atoms/button/button.component';
import { TextComponent } from './atoms/text/text.component';
import { LabelComponent } from './atoms/label/label.component';
import { ImageComponent } from './atoms/image/image.component';
import { TittleComponent } from './atoms/tittle/tittle.component';
import { InputComponent } from './atoms/input/input.component';
import { FormGroupComponent } from './molecules/form-group/form-group.component';

@NgModule({
  declarations: [
    NavbarComponent,
    BarbershopFormComponent,
    BarbershopListComponent,
    BabershopCardComponent,
    BabershopCardListComponent,
    LoaderComponent,
    InputValidDirective,
    ModalBarbershopEditComponent,
    AppointmentCardComponent,
    AppointmentFormComponent,
    AppointmentCardListComponent,
    AppointmentListComponent,
    ModalAppointmentEditComponent,
    ButtonComponent,
    TextComponent,
    LabelComponent,
    ImageComponent,
    TittleComponent,
    InputComponent,
    FormGroupComponent,
  ],
  imports: [CommonModule, RouterModule, ReactiveFormsModule],
  exports: [
    NavbarComponent,
    BarbershopFormComponent,
    BarbershopListComponent,
    BabershopCardComponent,
    BabershopCardListComponent,
    InputValidDirective,
    ModalBarbershopEditComponent,
    AppointmentCardComponent,
    AppointmentFormComponent,
    AppointmentCardListComponent,
    AppointmentListComponent,
    ModalAppointmentEditComponent,
  ],
})
export class SharedModule {}
