import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss'],
})
export class LabelComponent implements OnInit {
  @Input() text: string = '';
  @Input() marginTop: string = '';

  mtClassName: string = '';

  constructor() {}

  ngOnInit(): void {
    switch (this.marginTop) {
      case '2':
        this.mtClassName = 'mt-2';
        break;

      case '4':
        this.mtClassName = 'mt-4';
        break;

      case '6':
        this.mtClassName = 'mt-6';
        break;

      case '8':
        this.mtClassName = 'mt-8';
        break;

      case '10':
        this.mtClassName = 'mt-10';
        break;

      default:
        this.mtClassName = 'mt-2';
        break;
    }
  }
}
