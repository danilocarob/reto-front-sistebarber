import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit, OnChanges {
  @Input() size: string = '';
  @Input() type: string = '';
  @Input() text: string = '';
  @Input() icon: string = '';
  @Input() disabled: boolean = false;

  @Output('clickButton') click = new EventEmitter();

  typeClassName: string = '';
  sizeClassName: string = '';
  iconClassName: string = '';

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    //size button
    switch (this.size) {
      case 'small':
        this.sizeClassName = 'btn-sm';
        break;

      case 'big':
        this.sizeClassName = 'btn-lg';
        break;

      case 'normal':
        this.sizeClassName = '';
        break;

      default:
        this.sizeClassName = '';
        break;
    }

    //type button
    switch (this.type) {
      case 'primary':
        this.typeClassName = 'btn btn-primary';
        break;

      case 'secondary':
        this.typeClassName = 'btn btn-secondary';
        break;

      case 'success':
        this.typeClassName = 'btn btn-success';
        break;

      case 'info':
        this.typeClassName = 'btn btn-info';
        break;

      case 'warning':
        this.typeClassName = 'btn btn-warning';
        break;

      case 'danger':
        this.typeClassName = 'btn btn-danger';
        break;

      case 'light':
        this.typeClassName = 'btn btn-light';
        break;

      case 'dark':
        this.typeClassName = 'btn btn-dark';
        break;

      case 'link':
        this.typeClassName = 'btn btn-link';
        break;

      default:
        this.typeClassName = '';
        break;
    }

    this.iconClassName = this.icon;
  }

  onClick() {
    this.click.emit();
  }
}
