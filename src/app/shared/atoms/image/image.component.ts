import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {
  @Input() urlImg: string = '';
  @Input() size: string = '100';

  urlStyle: string = '';
  sizeClassName: string = '';

  constructor() {}

  ngOnInit(): void {
    this.urlStyle = `url(${this.urlImg})`;
  }
}
