import { Directive, ElementRef, DoCheck } from '@angular/core';

@Directive({
  selector: '[appInputValid]',
})
export class InputValidDirective implements DoCheck {
  private readonly INVALID = 'is-invalid';
  private readonly VALID = 'is-valid';
  private readonly NGVALID = 'ng-valid';
  private readonly NGINVALID = 'ng-invalid';
  private readonly NGTOUCHED = 'ng-touched';

  constructor(private elHost: ElementRef<HTMLElement>) {
    // elHost.nativeElement.classList.add('is-valid')
    // elHost.nativeElement.classList.add('is-invalid')
  }
  ngDoCheck(): void {
    if (this.contains([this.NGVALID])) {
      this.elHost.nativeElement.classList.add(this.VALID);
      this.elHost.nativeElement.classList.remove(this.INVALID);
    } else if (this.contains([this.NGINVALID, this.NGTOUCHED])) {
      console.log(this.elHost.nativeElement.className);

      this.elHost.nativeElement.classList.remove(this.VALID);
      this.elHost.nativeElement.classList.add(this.INVALID);
    }
  }

  contains(classes: Array<string>) {
    const splited = this.elHost.nativeElement.className.split(' ');
    return classes.reduce((acum, curr) => {
      return acum && splited.includes(curr);
    }, true);
  }
}
