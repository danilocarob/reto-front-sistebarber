import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarbershopFormComponent } from './barbershop-form.component';

describe('BarbershopFormComponent', () => {
  let component: BarbershopFormComponent;
  let fixture: ComponentFixture<BarbershopFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarbershopFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarbershopFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
