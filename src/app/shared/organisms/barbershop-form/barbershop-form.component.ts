import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BarbershopService } from 'src/app/services/barbershop.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-barbershop-form',
  templateUrl: './barbershop-form.component.html',
  styleUrls: ['./barbershop-form.component.scss'],
})
export class BarbershopFormComponent implements OnInit {
  public formBarbershop: FormGroup = new FormGroup({});
  @Input('editBarbershop') editBarbershop: any;
  public typeAction: 'update' | 'create' = 'create';
  public inputs: any;

  constructor(private _babershopService: BarbershopService) {
    this.inputs = [
      {
        type: 'text',
        name: 'nombre',
        label: 'Nombre',
        placeholder: 'Ingrese el nombre',
      },
      {
        type: 'text',
        name: 'direccion',
        label: 'Direccion',
        placeholder: 'Ingrese la dirección',
      },
      {
        type: 'number',
        name: 'telefono',
        label: 'Teléfono',
        placeholder: 'Ingrese el teléfono',
      },
      {
        type: 'select',
        name: 'estado',
        label: 'Estado',
        placeholder: 'Ingrese el estado',
        options: [
          { text: 'Activo', value: 'activo' },
          { text: 'Inactivo', value: 'inactivo' },
        ],
      },
    ];
  }

  ngOnInit(): void {
    if (this.editBarbershop) {
      //update
      this.typeAction = 'update';
      this.updateForm();
    } else {
      //create
      this.typeAction = 'create';
      this.createForm();
    }
  }

  submitBarbershop() {
    const barbershop = this.formBarbershop.value;

    if (this.formBarbershop.valid) {
      if (this.editBarbershop) {
        //update
        this.updateBarbershop(barbershop);
      } else {
        //create
        this.registerBarbershop(barbershop);
      }
    } else {
      this.formBarbershop.markAllAsTouched();
      Swal.fire('Atención', 'Revisa los campos que ingresaste', 'error');
    }
  }

  registerBarbershop(barbershop: any) {
    this._babershopService.saveBarbershop(barbershop).subscribe(
      (res) => {
        console.log(res);
        Swal.fire('Felicidades', 'Barbería registrada exitosamente', 'success');
        this._babershopService.newBarbershop$.next(barbershop);
        this.formBarbershop.reset();
      },
      (err) => {
        console.log(err);

        Swal.fire('Atención', 'Ocurrio un error', 'error');
      }
    );
  }

  updateBarbershop(barbershop: any) {
    this._babershopService
      .updateBarbershop(this.editBarbershop.id, barbershop)
      .subscribe(
        (res) => {
          Swal.fire(
            'Felicidades',
            'Barbería actualizada exitosamente',
            'success'
          );
          this._babershopService.newBarbershop$.next(barbershop);
          this.editBarbershop = '';
          this.formBarbershop.reset();
        },
        (err) => {
          Swal.fire('Atención', 'Ocurrio un error', 'error');
        }
      );
  }

  createForm() {
    this.formBarbershop = new FormGroup({
      nombre: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),

      direccion: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]),

      telefono: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),

      estado: new FormControl('', [Validators.required]),
    });
  }

  updateForm() {
    this.formBarbershop = new FormGroup({
      nombre: new FormControl(this.editBarbershop.nombre, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),

      direccion: new FormControl(this.editBarbershop.direccion, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]),

      telefono: new FormControl(this.editBarbershop.telefono, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),

      estado: new FormControl(this.editBarbershop.estado, [
        Validators.required,
      ]),
    });
  }
}
