import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppointmentService } from 'src/app/services/appointment.service';
import Swal from 'sweetalert2';
import { ModalAppointmentEditComponent } from '../modal-appointment-edit/modal-appointment-edit.component';

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss']
})
export class AppointmentListComponent implements OnInit {
  appointments: any = [];
  public id_barberia: any;
  public loading = false;

  constructor(
    private _appointmentService: AppointmentService,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private router: Router) 
    { }

  ngOnInit(): void {
    this.loading = true;
    const params = this.activatedRoute.snapshot.params;
    this.id_barberia = params.id;
    

    this._appointmentService.newAppointment$.subscribe(
      res =>{
        this.getAppointments(this.id_barberia);
        
      },
      err =>{
        console.log(err);
        
      }
    );

    this.getAppointments(this.id_barberia);
  }

  getAppointments(id_barberia: any){
  
    this._appointmentService.getAppointmentsById(id_barberia).subscribe(
      res =>{
        this.appointments = res;
        this.loading = false;
      },
      err => {
        // this.router.navigate(['/barberia']);
        console.log(err);
      }
    )
  }

  deleteAppointment(id : any){
    this._appointmentService.deleteAppointment(id).subscribe(
      res => {
        Swal.fire('Felicidades', 'Cita eliminada exitosamente', 'success');
        this.getAppointments(this.id_barberia);

      },
      err => {
        Swal.fire('Atención', 'Ocurrio un error', 'error');
      }
    )
  }

  editAppointment(appointment: any){

    const modalRef = this.modalService.open( ModalAppointmentEditComponent, {
      keyboard: true,
    });

    modalRef.componentInstance.editAppointment = appointment;
  }

}
