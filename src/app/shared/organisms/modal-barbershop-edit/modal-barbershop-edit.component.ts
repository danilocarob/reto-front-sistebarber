import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-barbershop-edit',
  templateUrl: './modal-barbershop-edit.component.html',
  styleUrls: ['./modal-barbershop-edit.component.scss']
})
export class ModalBarbershopEditComponent implements OnInit {
  @Input('editBarbershop') editBarbershop : any;

  constructor() { }

  ngOnInit(): void {
    console.log('este es la barberia a modificar', this.editBarbershop);
    
  }

}
