import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBarbershopEditComponent } from './modal-barbershop-edit.component';

describe('ModalBarbershopEditComponent', () => {
  let component: ModalBarbershopEditComponent;
  let fixture: ComponentFixture<ModalBarbershopEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalBarbershopEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBarbershopEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
