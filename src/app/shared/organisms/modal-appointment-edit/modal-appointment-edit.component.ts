import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-appointment-edit',
  templateUrl: './modal-appointment-edit.component.html',
  styleUrls: ['./modal-appointment-edit.component.scss']
})
export class ModalAppointmentEditComponent implements OnInit {
  @Input('editAppointment') editAppointment : any;
  constructor() { }

  ngOnInit(): void {
  }

}
