import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAppointmentEditComponent } from './modal-appointment-edit.component';

describe('ModalAppointmentEditComponent', () => {
  let component: ModalAppointmentEditComponent;
  let fixture: ComponentFixture<ModalAppointmentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAppointmentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAppointmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
