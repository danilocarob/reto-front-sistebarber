import { Component, OnInit } from '@angular/core';
import { BarbershopService } from 'src/app/services/barbershop.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalBarbershopEditComponent } from '../modal-barbershop-edit/modal-barbershop-edit.component';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-barbershop-list',
  templateUrl: './barbershop-list.component.html',
  styleUrls: ['./barbershop-list.component.scss']
})
export class BarbershopListComponent implements OnInit {
  barbershops: any = [];
  public loading: boolean = false;
  public validDelete : boolean = false;

  constructor(
    private _babershopService: BarbershopService,
    private _appointmentService: AppointmentService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.loading = true;
    this._babershopService.newBarbershop$.subscribe(
      res =>{
        this.getBarbershops();
      },
      err =>{}
    );

    this.getBarbershops();
    
  }

  getBarbershops(){
    this._babershopService.getAllBabershop().subscribe(
      res =>{
        this.barbershops = res;
        this.loading = false;
      },
      err => {
        console.log(err);
      }
    )
  }

  deleteBarbershop(id : any){
    // this.validateAppointments(id);
    // console.log('validelete',this.validDelete);
    if (this.validDelete) {
        this._babershopService.deleteBarbershop(id).subscribe(
          res => {
            Swal.fire('Felicidades', 'Barbería eliminada exitosamente', 'success');
            this.getBarbershops();
    
          },
          err => {
            Swal.fire('Atención', 'Ocurrio un error', 'error');
          }
        )
    } else {
      Swal.fire('Atención', 'Debes cancelar las citas', 'warning');
      this.validDelete = true;
    }


  }

  editBarbershop(barbershop: any){
console.log(barbershop);

    const modalRef = this.modalService.open( ModalBarbershopEditComponent, {
      keyboard: true,
    });

    modalRef.componentInstance.editBarbershop = barbershop;
  }

  //  validateAppointments(id :any) {
  //    console.log('este es el id', id);
     
  //       this._appointmentService.getAppointmentsById(id).subscribe(
  //           res =>{
  //               const appointments: any = res;
  //               console.log('citas', appointments.length);
    
  //               if (appointments.length >= 1) {
  //               this.validDelete = false;
  //               } else {
  //                 this.validDelete = true;
  //               }
              
  //           }
  //       );
  //   }

}
