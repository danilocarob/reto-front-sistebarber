import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppointmentService } from 'src/app/services/appointment.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-appointment-form',
  templateUrl: './appointment-form.component.html',
  styleUrls: ['./appointment-form.component.scss'],
})
export class AppointmentFormComponent implements OnInit {
  public formAppointment: FormGroup = new FormGroup({});
  @Input('editAppointment') editAppointment: any;
  public typeAction: 'update' | 'create' = 'create';
  public id_barberia: any;

  constructor(
    private _appointmentService: AppointmentService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const params = this.activatedRoute.snapshot.params;
    this.id_barberia = localStorage.setItem(
      'id_barberia',
      JSON.stringify(params.id)
    );

    if (this.editAppointment) {
      //update
      this.typeAction = 'update';
      this.updateForm();
    } else {
      //createt);
      this.typeAction = 'create';
      this.createForm();
    }
  }

  submitAppointment() {
    const appointment = this.formAppointment.value;

    if (this.formAppointment.valid) {
      if (this.editAppointment) {
        //update
        this.updateAppointment(appointment);
      } else {
        //create
        this.getAppointmentExist(appointment);
      }
    } else {
      this.formAppointment.markAllAsTouched();
      Swal.fire('Atención', 'Revisa los campos que ingresaste', 'error');
    }
  }

  registerAppointment(appointment: any) {
    const id_barberia = JSON.parse(localStorage.getItem('id_barberia') || '[]');

    this._appointmentService.saveAppointment(appointment).subscribe(
      (res) => {
        Swal.fire('Felicidades', 'Cita registrada exitosamente', 'success');
        this._appointmentService.newAppointment$.next(appointment);
        this.formAppointment.reset({
          nombre_completo: '',
          numero_documento: '',
          fecha_hora: '',
          telefono: '',
          barberia_id: parseInt(id_barberia),
        });
      },
      (err) => {
        Swal.fire('Atención', 'Ocurrio un error', 'error');
      }
    );
  }

  updateAppointment(appointment: any) {
    this._appointmentService
      .updateAppointment(this.editAppointment.id, appointment)
      .subscribe(
        (res) => {
          Swal.fire(
            'Felicidades',
            'Barbería actualizada exitosamente',
            'success'
          );
          this._appointmentService.newAppointment$.next(appointment);
          this.editAppointment = '';
          this.formAppointment.reset();
        },
        (err) => {
          Swal.fire('Atención', 'Ocurrio un error', 'error');
        }
      );
  }

  createForm() {
    const id_barberia = JSON.parse(localStorage.getItem('id_barberia') || '[]');

    this.formAppointment = new FormGroup({
      nombre_completo: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),

      numero_documento: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]),

      fecha_hora: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(30),
      ]),

      telefono: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),

      barberia_id: new FormControl(parseInt(id_barberia), [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),
    });
  }

  updateForm() {
    this.formAppointment = new FormGroup({
      nombre_completo: new FormControl(this.editAppointment.nombre_completo, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20),
      ]),

      numero_documento: new FormControl(this.editAppointment.numero_documento, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]),

      fecha_hora: new FormControl(this.editAppointment.fecha_hora, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(30),
      ]),

      telefono: new FormControl(this.editAppointment.telefono, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),

      barberia_id: new FormControl(this.editAppointment.barberia_id, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(15),
      ]),
    });
  }

  getAppointmentExist(appointment: any) {
    this._appointmentService.getAppointmentExist(appointment).subscribe(
      (res) => {
        Swal.fire(
          'Cita repetida',
          'Debes registrar una fecha y hora diferente',
          'warning'
        );
      },
      (err) => {
        this.registerAppointment(appointment);
      }
    );
  }
}
