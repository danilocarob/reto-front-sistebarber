import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
// import { data } from './../../../../assets/dataForms/barbershop.json';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
})
export class FormGroupComponent implements OnInit, OnChanges {
  @Input() inputs: any;
  @Input() form: FormGroup = new FormGroup({});
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    console.log('obj', this.inputs);

    console.log('formulario', this.form);
    this.form = new FormGroup(this.form.controls);
  }
}
