import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BabershopCardListComponent } from './babershop-card-list.component';

describe('BabershopCardListComponent', () => {
  let component: BabershopCardListComponent;
  let fixture: ComponentFixture<BabershopCardListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BabershopCardListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BabershopCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
