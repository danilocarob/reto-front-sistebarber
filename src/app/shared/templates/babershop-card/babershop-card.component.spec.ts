import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BabershopCardComponent } from './babershop-card.component';

describe('BabershopCardComponent', () => {
  let component: BabershopCardComponent;
  let fixture: ComponentFixture<BabershopCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BabershopCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BabershopCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
