import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentCardListComponent } from './appointment-card-list.component';

describe('AppointmentCardListComponent', () => {
  let component: AppointmentCardListComponent;
  let fixture: ComponentFixture<AppointmentCardListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentCardListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
